// Request a node to be allocated to us
node( "AndroidSDK" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First Thing: Checkout Sources
		stage('Checkout Sources') {
			// Make sure we have a clean slate to begin with
			deleteDir()

			// Krita Code
			checkout changelog: true, poll: true, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'krita/']],
				userRemoteConfigs: [[url: 'https://invent.kde.org/graphics/krita.git']]
			]

		}

		// Now retrieve the artifacts
		stage('Retrieving Dependencies') {
			// First we grab the artifacted dependencies built last time round
			copyArtifacts filter: 'krita-android-deps.tar', projectName: 'Krita_Android_arm64-v8a_Dependency_Build'

			// Now we unpack them
			sh """
				tar -xf $WORKSPACE/krita-android-deps.tar
			"""
		}

		// Make sure the Android SDK is setup properly
		stage('Setting up SDK') {
			// For this we need to ensure that the Android 24 or later SDK is installed, otherwise the APK generation will fail
			sh """
				sdkmanager "platforms;android-28"
			"""
		}

		// Let's build Krita that we have everything we need
		stage('Building Krita') {
			// The first parameter to the script is where the scripts should work - which is our workspace in this case
			// Otherwise we leave everything in the hands of that script
			sh """
				unset QT_ANDROID
				unset QMAKESPEC

				export ANDROID_ABI=arm64-v8a
				export ANDROID_API_LEVEL=23
				export CMAKE_ANDROID_NDK=\$ANDROID_NDK_ROOT
				export NIGHTLY_BUILD=1

				krita/packaging/android/androidbuild.sh --src=$WORKSPACE/krita/ --build-type=Release --build-root=$WORKSPACE/build/ -p=krita-bin
			"""
		}

		// Now we can generate the actual APKs!
		stage('Generating Krita APK') {
			// The scripts handle everything here, so just run them
			sh """
				unset QT_ANDROID
				unset QMAKESPEC

				export ANDROID_ABI=arm64-v8a
				export ANDROID_API_LEVEL=23
				export ANDROID_NDK_HOME=\$ANDROID_NDK_ROOT
				export CMAKE_ANDROID_NDK=\$ANDROID_NDK_ROOT
				export NIGHTLY_BUILD=1

				krita/packaging/android/androidbuild.sh --src=$WORKSPACE/krita/ --build-type=Release --build-root=$WORKSPACE/build/ -p=apk
				
				mv $WORKSPACE/build/krita_build_apk/build/outputs/apk/*/*.apk ./
			"""

			// Stash the APK
			stash includes: '*.apk', name: 'apks'
		}
	}
}
}

// With the APKs all built, we now need to transfer over to the signing machine as plain APKs won't be of much use to people
node( "AndroidSigner" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {
		// Time to sign!
		stage('Signing APKs') {
			// Make sure we have a clean environment first
			deleteDir()

			// Retrieve the APKs we captured earlier
			unstash 'apks'

			// Perform the signing process
			// By default we use the Keystore and associated details from ~/keys/android-signing-config
			// All APK files will be subject to zipalign prior to being signed in accordance with the Google documentation on signing of APKs
			sh """
				bash ~/binary-factory-tooling/android/perform-signing.sh
			"""
		}

		// Finally we capture the APKs for distribution to users
		stage('Capturing APKs') {
			// We use Jenkins artifacts for this to save having to setup additional infrastructure
			archiveArtifacts artifacts: '*.apk', onlyIfSuccessful: true
		}
	}
}
}
