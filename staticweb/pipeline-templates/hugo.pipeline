// Request a node to be allocated to us
node( "StaticWeb" ) {
// We want Timestamps on everything
timestamps {
    // We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
    catchError {
        // First Thing: Checkout Sources
        stage('Checkout Sources') {
            // Make sure we have a clean slate to begin with
            deleteDir()
            // Code
            checkout changelog: true, poll: true, scm: [
                $class: 'GitSCM',
                branches: [[name: gitBranch]],
                userRemoteConfigs: [[url: repositoryUrl]]
            ]
        }

        // Process data: translations, Planet posts, appdata, etc.
        stage('Process data') {
            sh """
                export LANG=en_US.UTF-8
                if [ -f "scripts/custom_generation.py" ]; then
                    python3 scripts/custom_generation.py
                else
                    echo "No data processing"
                fi
            """
        }

        // We host copies of some pages on www.kde.org for historical reasons
        stage('Fetch other resources') {
            if( "${deploypath}" == "/srv/www/generated/www.kde.org/" ) {
                sh """
                    curl "https://store.kde.org/content.rdf" -o static/kde-look-content.rdf

                    curl "https://dot.kde.org/rss.xml" -o static/dot/rss.xml
                    cp -f static/dot/rss.xml static/dotkdeorg.rdf
                """
            }
            if( "${deploypath}" == "/srv/www/generated/develop.kde.org/" ) {
                sh """
                    # Fetch config keys from some plasma repos
                    python3 scripts/extract-plasma-applet-config-keys.py
                """
            }
        }

        // Let's build website now
        stage('Build website') {
            sh """
                # Download postcss plugins for RTL support
                hugo mod npm pack
                npm install

                if [ "${name}" = "www-staging-kde-org" ]; then
                    env HUGO_BASEURL="https://www-staging.kde.org/" hugo --minify --buildFuture --buildDrafts
                else
                    hugo --minify
                fi
            """
        }

        // Deploy the website!
        stage('Publish website') {
            sh """
                rsync -Hrlpvc --delete -e "ssh -i ${deploykey}" public/ ${deployuser}@${deployhost}:${deploypath}
            """
        }
    }
}
}
